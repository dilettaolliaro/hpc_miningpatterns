#include "panda.hpp"

//A lot of commented line are actually chrono instructions to measure execution times

int main() {
    //auto start2 = std::chrono::steady_clock::now();
    //std::chrono::duration<double> total, program;

    std::string input = "Datasets/chess.dat";
    datas dataset = get_dataset(input);

    //auto start = std::chrono::steady_clock::now();

    //call to the PaNDa+ algorithm
    pattern_set patterns = panda_plus(4, dataset);
    
    //auto end = std::chrono::steady_clock::now();
    //total = std::chrono::duration_cast<std::chrono::duration<float>>(end-start);

    //std::cout << "Panda: " << total.count() << std::endl;
 
    for (auto &&p : patterns.p) {
        print_pattern(p);        
    }
    std::cout<<std::endl;
    std::cout<<"This set of patterns has: "<<std::endl;
    std::cout<<patterns.q.falseNegatives<<" false negatives"<<std::endl;
    std::cout<<patterns.q.falsePositives<<" false positives"<<std::endl;
    std::cout<<patterns.q.complexity<<" complexity"<<std::endl<<std::endl;
    
    //auto end2 = std::chrono::steady_clock::now();
    //program = std::chrono::duration_cast<std::chrono::duration<float>>(end2-start2);
    //std::cout << "Program: " << program.count() << std::endl;


}

