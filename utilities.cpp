#include "utilities.hpp"

datas get_dataset(std::string input){
    std::ifstream infile(input);
    std::string file_row;
    datas dataset;
    int index=0;
    while (std::getline(infile, file_row)) {
        std::stringstream stream(file_row);
        int num;
        std::vector<int> elems;

        while (stream >> num) {
            elems.push_back(num);
            if (stream.peek() == ' '){
                stream.ignore();
            }
        }
        dataset.push_back({index, elems});
        index++;
    }
    return dataset;
}

void print_pattern(pattern p){
    std::cout<<"This pattern has items: ";
    sort(p.items.begin(), p.items.end());
    for (int i = 0; i < p.items.size(); i++) {
        std::cout << p.items[i] << " ";
    }
    std::cout<<std::endl;
    std::cout<<"Number of transactions: "<<p.transactions.size()<<std::endl;
}

int get_size(datas dataset){
    int dim=0;
    for (auto &&r : dataset){
        dim+=r.items.size();   
    }
    return dim;
}

bool compare(std::pair<int,int> item1, std::pair<int, int> item2) { 
    return (item1.second > item2.second); 
} 

std::vector<int> sorting(datas dataset){
    std::vector<int> sortedItems;
    std::vector<std::pair<int, int>> sorting; 
    std::map<int, int> freq = getFrequencies(dataset);
    
    for(auto &&item : freq){
        sorting.push_back(item);
    }

    sort(sorting.begin(), sorting.end(), compare);

    for (auto &&item : sorting){
        sortedItems.push_back(item.first);
    }
   return sortedItems;
}

std::map<int, int> getFrequencies(datas dataset){
    std::map<int, int> frequencies;
    for (auto &&row : dataset){
        for (auto &&elem : row.items){
            auto iter = frequencies.find(elem);
            if(iter != frequencies.end()){
                iter->second++;
            }else{
                frequencies.insert(std::pair<int, int>(elem, 1));
            }
        }
    }
    return frequencies;
}

bool residuals_are_empty(datas residuals){
    bool flag = true;
    for (int re = 0; re < residuals.size(); re++) {
        row r = residuals[re];
        if(!(r.items.empty())){
            flag = false;
        }
    }
    return flag;
}

void get_residuals(pattern_set patterns, datas &dataset){
    for (auto &&p : patterns.p) {
        for (auto &&t : p.transactions) {
            std::vector<int> r;
            for (auto &&i : dataset[t].items) {
                if(count(p.items.begin(), p.items.end(), i)==0){
                    r.push_back(i);
                }
            }
            dataset[t].items = r;
        }
    }
}

