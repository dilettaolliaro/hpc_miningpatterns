#include "quality.hpp"
#include <iostream>

//function that computes the cost of a set of pattern
float cost(patterns_quality p){
    float rho=0.5; 
    int noise=0;
    //noise is given by the sum of false negatives and false positives
    noise = p.falseNegatives+p.falsePositives;
    return p.complexity*rho+noise;
}

bool not_too_noisy(datas dataset, pattern p, float eps_r, float eps_c) {

    bool flag = true;
    int itCounter, trCounter;
    //this gives the threshold for items
    int itThreshold = (1-eps_c)*p.transactions.size();
    //this gives the threshold for transactions
    int trThreshold = (1-eps_r)*p.items.size();
    
    //constraint on items: every item must be included in at least -itThreshold- transactions of P
    #pragma omp parallel for
    //for every item
    for (int i = 0; i < p.items.size(); i++) {
        itCounter = 0;
        auto it = p.items[i];
        //for every transaction
        for (int t = 0; t < p.transactions.size(); t++) {
            auto tr = p.transactions[t];
            std::vector<int> v = dataset[tr].items;
            if(std::find(v.begin(), v.end(), it) != v.end()) {
                #pragma omp atomic
                //we count one if item is included in transaction
                itCounter++;
            }
        }
        //we check if the constraint on items is met for every item, if it is not flag goes to false
        if (itCounter < itThreshold) {
            flag = false;
        }
    }
    //constraint on transactions: every transaction must include at least -trThreshold- item of P
    #pragma omp parallel for
    //for every transaction
    for (int t = 0; t < p.transactions.size(); t++) {
        trCounter = 0;
        auto tr = p.transactions[t];
        auto v = dataset[tr].items;
        //for every  item
        for (int i = 0; i < p.items.size(); i++) {
            auto it = p.items[i];
            //we count one if the transaction includes the item
            if (std::find(v.begin(), v.end(), it) != v.end()) {
                #pragma omp atomic
                trCounter++;
            }
        }
        //we check if the constraint on transactions is met for every transactions, if it is not flag goes to false
        if (trCounter < trThreshold) {
            flag = false;
        }        
    }

    return flag;
}