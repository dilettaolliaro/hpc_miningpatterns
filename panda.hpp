#ifndef PANDA_H
#define PANDA_H

#include "utilities.hpp"
#include "quality.hpp"

//main algorithm
pattern_set panda_plus(int k, datas dataset);

//first step of PaNDa+ algorithm: find a dense core that does not admit false positives
std::pair<pattern, std::queue<int>> find_core(datas residuals, patterns_quality &possibleQuality, datas dataset);

//second step of PaNDa+ algorithm: extend the dense core admitting false positives
pattern extend_core(pattern core, std::queue<int> &ext_list, patterns_quality &possibleQuality, datas dataset, datas residuals, float eps_r, float eps_c);

#endif