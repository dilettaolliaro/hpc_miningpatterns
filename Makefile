CC = gcc

CFLAGS = -std=c++17 -Xpreprocessor -fopenmp -lomp -lstdc++

mining: main.o panda.o quality.o utilities.o

	$(CC) $(CFLAGS) -o mining main.o panda.o quality.o utilities.o

main.o: main.cpp panda.hpp utilities.hpp quality.hpp

	$(CC) $(CFLAGS) -c main.cpp

panda.o: panda.cpp panda.hpp 

	$(CC) $(CFLAGS) -c panda.cpp

quality.o: quality.cpp quality.hpp structures.hpp

	$(CC) $(CFLAGS) -c quality.cpp

utilities.o: utilities.cpp utilities.hpp structures.hpp 

	$(CC) $(CFLAGS) -c utilities.cpp

clean:

	-rm *.o *.s
