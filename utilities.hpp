#ifndef UTILITIES_H
#define UTILITIES_H

#include "structures.hpp"
#include <fstream>
#include <sstream>
#include <iostream>
#include <chrono>

//take input dataset
datas get_dataset(std::string input);

//print a pattern
void print_pattern(pattern p);

//returns size of a dataset
int get_size(datas dataset);

//sorts by frequency
std::vector<int> sorting(datas dataset);

//retrieve frequencies of each item and creates a map item-frequency
std::map<int, int> getFrequencies(datas dataset);

//checks if the residuals are empty
bool residuals_are_empty(datas residuals);

//gets residuals (portions of dataset not covered by a pattern)
void get_residuals(pattern_set patterns, datas &dataset);

#endif