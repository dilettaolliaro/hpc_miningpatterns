// gcc test.cpp panda.cpp quality.cpp utilities.cpp -std=c++2a -lstdc++ -o test
// ./test

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#include "panda.hpp"
#include "structures.hpp"

template <typename A, typename T>
bool compareArr(const A &a, const std::initializer_list<T> &b) {
  auto aIter = a.begin();
  auto bIter = b.begin();
  while (aIter != a.end() && bIter != b.end()) {
    if (*aIter != *bIter) return false;
    aIter++;
    bIter++;
    if (aIter == a.end() && bIter != b.end()) return false;
    if (aIter != a.end() && bIter == b.end()) return false;
  }
  return true;
}

template <typename A, typename T>
bool compareArrUnordered(const A &a, const std::initializer_list<T> &b) {
  if (a.size() != b.size()) return false;
  for (auto &&i : b) {
    if (std::find(a.cbegin(), a.cend(), i) == a.cend()) {
      return false;
    }
  }

  return true;
}

template <typename A, typename T>
bool compareSet(const A &a, const std::initializer_list<T> &b) {
  for (auto &&i : b) {
    if (a.count(i) == 0) return false;
  }
  return a.size() == b.size();
}

// TEST_CASE("notTooNoisy") {
//   SUBCASE("Dense") {
//     std::vector<row> dataset;
//     dataset.push_back({0, {0, 1, 2}});
//     dataset.push_back({1, {0, 1, 2}});
//     Pattern pattern({0, 1, 2}, {0, 1});
//     CHECK(notTooNoisy(dataset, pattern, 1.0, 1.0));
//     CHECK(notTooNoisy(dataset, pattern, 0.001, 0.001));
//   }
//   SUBCASE("Sparse 1") {
//     std::vector<row> dataset;
//     dataset.push_back({0, {0, 1, 2}});
//     dataset.push_back({1, {0, 1, 2}});
//     dataset.push_back({2, {0, 1}});
//     Pattern pattern({0, 1, 2}, {0, 1, 2});
//     CHECK(notTooNoisy(dataset, pattern, 1.0, 1.0));
//     CHECK(!notTooNoisy(dataset, pattern, 0.001, 1.0));
//   }
//   SUBCASE("Sparse 2") {
//     std::vector<row> dataset;
//     dataset.push_back({0, {0, 1, 2}});
//     dataset.push_back({1, {0, 1}});
//     dataset.push_back({2, {3, 4, 5}});
//     Pattern pattern({0, 1, 2}, {0, 1, 2});
//     CHECK(notTooNoisy(dataset, pattern, 1.0, 1.0));
//     CHECK(!notTooNoisy(dataset, pattern, 1.0, 0.001));
//   }
// }

TEST_CASE("find_core") {
  SUBCASE("Dense") {
    std::vector<row> dataset;
    dataset.push_back({0, {0, 1, 2}});
    dataset.push_back({1, {0, 1, 2}});
    pattern_set patterns;
    patterns_quality quality;
    quality.falseNegatives = get_size(dataset);
    auto [core, extensionList] = find_core(dataset, quality, dataset);
    CHECK(quality.falseNegatives == 0);
    CHECK(compareSet(core.items, {0, 1, 2}));
    CHECK(compareArrUnordered(core.transactions, {0, 1}));
  }
  SUBCASE("Sparse 1") {
    std::vector<row> dataset;
    dataset.push_back({0, {0, 1, 2}});
    dataset.push_back({1, {0, 1}});
    dataset.push_back({2, {0, 1}});
    pattern_set patterns;
    patterns_quality quality;
    quality.falseNegatives = get_size(dataset);
    auto [core, extensionList] = find_core(dataset, quality, dataset);
    CHECK(quality.falseNegatives == 1);
    CHECK(compareSet(core.items, {0, 1}));
    CHECK(compareArrUnordered(core.transactions, {0, 1, 2}));
  }
  SUBCASE("Sparse 2") {
    std::vector<row> dataset;
    dataset.push_back({0, {0, 1, 2}});
    dataset.push_back({1, {0, 1, 2, 3}});
    dataset.push_back({2, {0}});
    pattern_set patterns;
    patterns_quality quality;
    quality.falseNegatives = get_size(dataset);
    auto [core, extensionList] = find_core(dataset, quality, dataset);
    CHECK(quality.falseNegatives == 2);
    CHECK(compareSet(core.items, {0, 1, 2}));
    CHECK(compareArrUnordered(core.transactions, {0, 1}));
  }
  SUBCASE("Sparse 3") {
    std::vector<row> dataset;
    dataset.push_back({0, {0, 1, 2}});
    dataset.push_back({1, {0, 1, 2, 3}});
    dataset.push_back({2, {0}});
    dataset.push_back({3, {0, 1}});
    dataset.push_back({4, {1, 0}});
    dataset.push_back({5, {1, 2}});
    pattern_set patterns;
    patterns_quality quality;
    quality.falseNegatives = get_size(dataset);
    auto [core, extensionList] = find_core(dataset, quality, dataset);
    CHECK(quality.falseNegatives == 6);
    CHECK(compareSet(core.items, {0, 1}));
    CHECK(compareArrUnordered(core.transactions, {0, 1, 3, 4}));
  }
  SUBCASE("Multiple 1") {
    std::vector<row> dataset;
    dataset.push_back({0, {0, 1, 2, 3}});
    dataset.push_back({1, {0, 1, 2, 3}});
    dataset.push_back({2, {0, 1, 2, 3}});
    dataset.push_back({3, {0, 1}});
    dataset.push_back({4, {0, 1}});
    dataset.push_back({5, {1, 0}});
    dataset.push_back({6, {1, 2}});
    pattern_set patterns;
    patterns_quality quality;
    quality.falseNegatives = get_size(dataset);
    auto [core, extensionList] = find_core(dataset, quality, dataset);
    CHECK(quality.falseNegatives == 8);
    CHECK(compareSet(core.items, {0, 1}));
    CHECK(compareArrUnordered(core.transactions, {0, 1, 2, 3, 4, 5}));
    patterns.p.push_back(core);
    get_residuals(patterns, dataset);

    auto [core2, extensionList2] = find_core(dataset, quality, dataset);
    CHECK(quality.falseNegatives == 2);
    CHECK(compareSet(core2.items, {2, 3}));
    CHECK(compareArrUnordered(core2.transactions, {0, 1, 2}));
  }
}

TEST_CASE("extend_core") {
  SUBCASE("Dense") {
    std::vector<row> dataset;
    dataset.push_back({0, {0, 1, 2}});
    dataset.push_back({1, {0, 1, 2}});
    pattern_set patterns;
    patterns_quality quality;
    quality.falseNegatives = get_size(dataset);
    auto [core, extensionList] = find_core(dataset, quality, dataset);
    auto extendedCore =
        extend_core(core, extensionList, quality, dataset, dataset, 1.0, 1.0);
    CHECK(quality.falsePositives == 0);
    CHECK(quality.falseNegatives == 0);
    CHECK(compareSet(extendedCore.items, {0, 1, 2}));
    CHECK(compareArrUnordered(extendedCore.transactions, {0, 1}));
  }
  SUBCASE("Sparse 1") {
    std::vector<row> dataset;
    dataset.push_back({0, {0, 1, 2}});
    dataset.push_back({1, {0, 1, 2}});
    dataset.push_back({2, {0, 1, 2}});
    dataset.push_back({3, {0, 1, 2}});
    dataset.push_back({4, {0, 1}});
    pattern_set patterns;
    patterns_quality quality;
    quality.falseNegatives = get_size(dataset);
    auto [core, extensionList] = find_core(dataset, quality, dataset);
    auto extendedCore =
        extend_core(core, extensionList, quality, dataset, dataset, 1.0, 1.0);
    CHECK(quality.falsePositives == 1);
    CHECK(quality.falseNegatives == 0);
    CHECK(compareSet(extendedCore.items, {0, 1, 2}));
    CHECK(compareArrUnordered(extendedCore.transactions, {0, 1, 2, 3, 4}));
  }
  SUBCASE("Sparse 2") {
    std::vector<row> dataset;
    dataset.push_back({0, {0, 1, 2}});
    dataset.push_back({1, {0, 1, 2, 3}});
    dataset.push_back({2, {0}});
    pattern_set patterns;
    patterns_quality quality;
    quality.falseNegatives = get_size(dataset);
    auto [core, extensionList] = find_core(dataset, quality, dataset);
    auto extendedCore =
        extend_core(core, extensionList, quality, dataset, dataset, 1.0, 1.0);
    CHECK(quality.falsePositives == 0);
    CHECK(quality.falseNegatives == 2);
    CHECK(compareSet(extendedCore.items, {0, 1, 2}));
    CHECK(compareArrUnordered(extendedCore.transactions, {0, 1}));
  }
  SUBCASE("Sparse 3") {
    std::vector<row> dataset;
    dataset.push_back({0, {0, 1, 2}});
    dataset.push_back({1, {0, 1, 2, 3}});
    dataset.push_back({2, {0}});
    dataset.push_back({3, {0, 1}});
    dataset.push_back({4, {1, 0}});
    dataset.push_back({5, {1, 2}});
    pattern_set patterns;
    patterns_quality quality;
    quality.falseNegatives = get_size(dataset);
    auto [core, extensionList] = find_core(dataset, quality, dataset);
    auto extendedCore =
        extend_core(core, extensionList, quality, dataset, dataset, 1.0, 1.0);
    CHECK(quality.falsePositives == 0);
    CHECK(quality.falseNegatives == 6);
    CHECK(compareSet(extendedCore.items, {0, 1}));
    CHECK(compareArrUnordered(extendedCore.transactions, {0, 1, 3, 4}));
  }
  SUBCASE("Sparse 4") {
    std::vector<row> dataset;
    dataset.push_back({0, {0, 1, 2, 3}});
    dataset.push_back({1, {0, 1, 2, 3}});
    dataset.push_back({2, {0, 1, 2, 3}});
    dataset.push_back({3, {0, 1, 2, 3}});
    dataset.push_back({4, {0, 1, 2, 3}});
    dataset.push_back({5, {0, 1, 2, 3}});
    dataset.push_back({6, {0, 1, 2, 3}});
    dataset.push_back({7, {0, 1, 2, 3}});
    dataset.push_back({8, {0, 2, 3}});
    dataset.push_back({9, {0, 1, 2, 3}});
    dataset.push_back({10, {0, 1, 2, 3}});
    dataset.push_back({11, {0, 1, 2, 3}});
    pattern_set patterns;
    patterns_quality quality;
    quality.falseNegatives = get_size(dataset);
    auto [core, extensionList] = find_core(dataset, quality, dataset);
    auto extendedCore =
        extend_core(core, extensionList, quality, dataset, dataset, 1.0, 1.0);
    CHECK(quality.falsePositives == 1);
    CHECK(quality.falseNegatives == 0);
    CHECK(compareSet(extendedCore.items, {0, 1, 2, 3}));
    CHECK(compareArrUnordered(extendedCore.transactions,
                              {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}));
  }
  SUBCASE("Sparse 5") {
    std::vector<row> dataset;
    dataset.push_back({0, {0, 1, 2}});
    dataset.push_back({1, {0, 1, 2}});
    dataset.push_back({2, {0, 1, 2}});
    dataset.push_back({3, {0, 1, 2}});
    dataset.push_back({4, {0, 1, 2}});
    dataset.push_back({5, {0, 1, 2}});
    dataset.push_back({6, {0, 1, 2}});
    dataset.push_back({7, {0, 1, 2}});
    dataset.push_back({8, {0, 1}});
    dataset.push_back({9, {0, 1, 2}});
    dataset.push_back({10, {0, 1, 2}});
    dataset.push_back({11, {0, 1, 2}});
    pattern_set patterns;
    patterns_quality quality;
    quality.falseNegatives = get_size(dataset);
    auto [core, extensionList] = find_core(dataset, quality, dataset);
    auto extendedCore =
        extend_core(core, extensionList, quality, dataset, dataset, 1.0, 1.0);
    CHECK(quality.falsePositives == 1);
    CHECK(quality.falseNegatives == 0);
    CHECK(compareSet(extendedCore.items, {0, 1, 2}));
    CHECK(compareArrUnordered(extendedCore.transactions,
                              {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}));
  }
  SUBCASE("Multiple 1") {
    std::vector<row> dataset;
    dataset.push_back({0, {0, 1, 2, 3}});
    dataset.push_back({1, {0, 1, 2, 3}});
    dataset.push_back({2, {0, 1, 2, 3}});
    dataset.push_back({3, {0, 1}});
    dataset.push_back({4, {0, 1}});
    dataset.push_back({5, {1, 0}});
    dataset.push_back({6, {1, 2}});
    std::vector<row> residuals = dataset;
    pattern_set patterns;
    patterns_quality qualityTest;
    qualityTest.falseNegatives = get_size(dataset);
    auto [core, extensionList] = find_core(residuals, qualityTest, dataset);
    auto extendedCore = extend_core(core, extensionList, qualityTest, dataset, residuals, 1.0, 1.0);
    CHECK(qualityTest.falsePositives == 0);
    CHECK(qualityTest.falseNegatives == 8);
    CHECK(compareSet(extendedCore.items, {0, 1}));
    CHECK(compareArrUnordered(extendedCore.transactions, {0, 1, 2, 3, 4, 5}));
    print_pattern(extendedCore);
    patterns.p.push_back(extendedCore);
    get_residuals(patterns, residuals);


    auto [core2, extensionList2] = find_core(residuals, qualityTest, dataset);
    auto extendedCore2 = extend_core(core2, extensionList, qualityTest, dataset,
                                     residuals, 1.0, 1.0);
    CHECK(qualityTest.falsePositives == 0);
    CHECK(qualityTest.falseNegatives == 2);
    CHECK(compareSet(extendedCore2.items, {2, 3}));
    CHECK(compareArrUnordered(extendedCore2.transactions, {0, 1, 2}));
  }
  // SUBCASE("Multiple 2") {
  //   std::vector<row> dataset;
  //   dataset.push_back({0, {0, 1, 2, 3, 4}});
  //   dataset.push_back({1, {0, 1, 2, 3, 4}});
  //   dataset.push_back({2, {0, 1, 2, 3, 4}});
  //   dataset.push_back({3, {0, 1, 2, 3, 4}});
  //   dataset.push_back({4, {0, 1, 2, 4}});
  //   dataset.push_back({5, {0, 1}});
  //   dataset.push_back({6, {0, 1}});
  //   dataset.push_back({7, {0, 1}});
  //   dataset.push_back({8, {0}});
  //   dataset.push_back({9, {0, 1}});
  //   dataset.push_back({10, {0, 1}});
  //   dataset.push_back({11, {0, 1}});
  //   pattern_set patterns;
  //   patterns_quality quality;
  //   quality.falseNegatives = get_size(dataset);
  //   auto [core, extensionList] = find_core(dataset, quality, dataset);
  //   auto [extendedCore, resultFalsePositivesE, resultFalseNegativesE] =
  //       extend_core(patterns, dataset, core, extensionList,
  //                   resultFalseNegatives, 0, 1.0, 1.0, 0.5);
  //   CHECK(resultFalsePositivesE == 0);
  //   CHECK(quality.falseNegativesE == 15);
  //   CHECK(compareSet(extendedCore.items, {0, 1}));
  //   CHECK(compareArrUnordered(extendedCore.transactions,
  //                             {0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11}));
  //   patterns.p.push_back(extendedCore);
  //   get_residuals(patterns, dataset);

  //   auto [core2, extensionList2, resultFalseNegatives2] =
  //       find_core(dataset, quality, dataset);
  //   auto [extendedCore2, resultFalsePositivesE2, resultFalseNegativesE2] =
  //       extend_core(patterns, dataset, core2, extensionList2,
  //                   resultFalseNegatives2, resultFalsePositivesE, 1.0, 1.0,
  //                   0.5);
  //   CHECK(resultFalsePositivesE2 == 1);
  //   CHECK(quality.falseNegativesE2 == 1);
  //   CHECK(compareSet(extendedCore2.items, {2, 3, 4}));
  //   CHECK(compareArrUnordered(extendedCore2.transactions, {0, 1, 2, 3, 4}));
  // }
  // SUBCASE("Multiple 3") {
  //   std::vector<row> dataset;
  //   dataset.push_back({0, {0, 1, 2}});
  //   dataset.push_back({1, {0, 1, 2}});
  //   dataset.push_back({2, {0, 1, 2, 3, 4, 5}});
  //   dataset.push_back({3, {0, 1, 2, 3, 4, 5}});
  //   dataset.push_back({4, {1, 2, 3, 4, 5}});
  //   dataset.push_back({5, {1, 2, 3, 4, 5}});
  //   dataset.push_back({6, {1, 2, 3, 4, 5, 6, 7, 8}});
  //   dataset.push_back({7, {4, 5, 6, 7, 8}});
  //   pattern_set patterns;
  //   patterns_quality quality;
  //   quality.falseNegatives = get_size(dataset);
  //   auto [core, extensionList] = find_core(dataset, quality, dataset);
  //   auto [extendedCore, resultFalsePositivesE, resultFalseNegativesE] =
  //       extend_core(patterns, dataset, core, extensionList,
  //                   resultFalseNegatives, 0, 1.0, 1.0, 0.5);
  //   CHECK(resultFalsePositivesE == 0);
  //   CHECK(quality.falseNegativesE == 16);
  //   CHECK(compareSet(extendedCore.items, {1, 2, 3, 4, 5}));
  //   CHECK(compareArrUnordered(extendedCore.transactions, {2, 3, 4, 5, 6}));
  //   patterns.p.push_back(extendedCore);
  //   get_residuals(patterns, dataset);

  //   auto [core2, extensionList2, resultFalseNegatives2] =
  //       find_core(dataset, quality, dataset);
  //   auto [extendedCore2, resultFalsePositivesE2, resultFalseNegativesE2] =
  //       extend_core(patterns, dataset, core2, extensionList2,
  //                   resultFalseNegatives2, resultFalsePositivesE, 1.0, 1.0,
  //                   0.5);
  //   CHECK(resultFalsePositivesE2 == 0);
  //   CHECK(quality.falseNegativesE2 == 8);
  //   CHECK(compareSet(extendedCore2.items, {0, 1, 2}));
  //   CHECK(compareArrUnordered(extendedCore2.transactions, {0, 1, 2, 3}));
  //   patterns.p.push_back(extendedCore2);
  //   get_residuals(patterns, dataset);

  //   auto [core3, extensionList3, resultFalseNegatives3] =
  //       find_core(dataset, quality, dataset);
  //   auto [extendedCore3, resultFalsePositivesE3, resultFalseNegativesE3] =
  //       extend_core(patterns, dataset, core3, extensionList3,
  //                   resultFalseNegatives3, resultFalsePositivesE2, 1.0, 1.0,
  //                   0.5);
  //   CHECK(resultFalsePositivesE3 == 0);
  //   CHECK(quality.falseNegativesE3 == 0);
  //   CHECK(compareSet(extendedCore3.items, {4, 5, 6, 7, 8}));
  //   CHECK(compareArrUnordered(extendedCore3.transactions, {6, 7}));
  //   patterns.p.push_back(extendedCore3);
  //   get_residuals(patterns, dataset);
  // }
}
