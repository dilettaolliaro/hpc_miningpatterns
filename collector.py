import subprocess
import sys 
import re
import csv

n = 3

r1 = re.compile(r"ext_first: (.*)")
r2 = re.compile(r"not_too_noisy: (.*)")
r3 = re.compile(r"find_core: (.*)")
r4 = re.compile(r"ext_core: (.*)")
r5 = re.compile(r"get_residuals: (.*)")
r6 = re.compile(r"Panda: (.*)")

ext_first = []
not_too_noisy = []
find_core = []
ext_core = []
get_residuals = []
panda = []

for x in range(0,20):
	extFirst = 0
	noisy = 0
	out = subprocess.check_output("./mining", shell=True)
	out = out.decode()
	for line in out.split("\n\n"):
		if "ext_first" in line:
			m = re.match(r1, line)
			extFirst += float(m.group(1))
		if "not_too_noisy" in line:
			m = re.match(r2, line)
			noisy += float(m.group(1))
		if "find_core" in line:
			m = re.match(r3, line)
			find = float(m.group(1))
			find_core.append(find)
		if "ext_core" in line:
			m = re.match(r4, line)
			ext = float(m.group(1))
			ext_core.append(ext)
		if "get_residuals" in line:
			m = re.match(r5, line)
			getres = float(m.group(1))
			get_residuals.append(getres)
		if "Panda" in line:
			m = re.match(r6, line)
			p = float(m.group(1))
			panda.append(p)

	ext_first.append(extFirst)
	not_too_noisy.append(noisy)
	x+=1

ext_first = [round(x,n) for x in ext_first]
not_too_noisy = [round(x, n) for x in not_too_noisy]
find_core = [round(x, n) for x in find_core]
ext_core = [round(x, n) for x in ext_core]
get_residuals = [round(x,n) for x in get_residuals]
panda = [round(x,n) for x in panda] 


headers = ["ext_first", "not_too_noisy", "find_core", "ext_core", "get_residuals", "panda"]

rows = zip(ext_first, not_too_noisy, find_core, ext_core, get_residuals, panda)
with open("6.SetImplementationStats.csv", "w") as f:
	writer = csv.writer(f)
	writer.writerow(headers)
	for row in rows:
		writer.writerow(row)