#ifndef STRUCTURES_H
#define STRUCTURES_H

#include <cfloat>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <vector>

//pattern data structure contains two vectors, one containing items and the other one
//containing transactions.

struct pattern {
    std::vector<int> items;
    std::vector<int> transactions;
};

//patterns_quality data structure contains all the data necessary to figure out
//if it is convenient or not to add a cerain item/transaction/pattern

struct patterns_quality {
    int falseNegatives;
    int falsePositives;
    int complexity; //number of items plus number of transactions

        patterns_quality(){
            falsePositives = 0;
            complexity = 0;
        }
};

//data structure pattern_set contains both the actual vector of pattern and
//the data to obtain the quality they achieve

struct pattern_set {
    std::vector<pattern> p;
    patterns_quality q;
};

struct row {
    int index; //indice originale della riga
    std::vector<int> items; //riga contenente indici delle colonne accese
};

typedef std::vector<row> datas;


#endif 
