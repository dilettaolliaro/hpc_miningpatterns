
#ifndef QUALITY_H
#define QUALITY_H

#include "structures.hpp"

float cost(patterns_quality p);

bool not_too_noisy(datas dataset, pattern p, float eps_r, float eps_c);

#endif