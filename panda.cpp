#include "panda.hpp"



std::vector<int> find_trans(datas dataset, pattern core){
    auto v = core.transactions;
    std::vector<int> remainingTrans;
    //for each transaction in the dataset if it is not contained in the core
    //transactions set then we added to the remainingTrans vector
    for (auto &&tr : dataset) {
        if (count(v.begin(), v.end(), tr.index) == 0) {
            remainingTrans.push_back(tr.index);
        }
    }
    return remainingTrans;
}

pattern extend_core(pattern core, std::queue<int> &ext_list, patterns_quality &possibleQuality, datas dataset, datas residuals, float eps_r, float eps_c){

    //std::chrono::duration<double> noise1, noise2;
    
    bool addedItem = true;
    patterns_quality currentPatterns = possibleQuality, provPatterns = possibleQuality, intermediate; 
    float currentCost, provCost; 
    pattern prov;

    currentCost = cost(currentPatterns);

    while(addedItem){
        //we collect all the yet not covered transactions
        std::vector<int> newTrans = find_trans(dataset, core);

        //for each one of these
        for (auto &&tr : newTrans) {

            prov = core;

            //we add it to the candidate pattern transactions set
            prov.transactions.push_back(tr);
            //we update complexity by one because we added one transaction
            provPatterns.complexity++;

            //for each item already in the pattern
            for (auto &&it : core.items) {
            
                //we check if the item is actually included in the transaction 
                const bool included = (std::find(dataset[tr].items.begin(), dataset[tr].items.end(), it) != dataset[tr].items.end());
                //we check if the item is already covered by another pattern 
                //(to do so we check if it is in the residuals if it is not, it is already covered)
                const bool covered = (std::find(residuals[tr].items.begin(), residuals[tr].items.end(), it) == residuals[tr].items.end());
                //if the item is not already covered by another pattern but it is included in the dataset
                //then we have one less false negative since thanks to the addition of the new transaction
                //that item is now covered
                if (!covered && included) {
                    provPatterns.falseNegatives--;
                //otherwise if it is not included we increase by one false positives since what we are doing
                //is equivalent to cover an item-transaction pair that does not really exist.
                } else if (!included) {
                    provPatterns.falsePositives++;
                }
            }

            //we compute candidate core
            provCost = cost(provPatterns);
            
            //if the cost has improved and the new pattern is not too noisy
            //we keep the candidate and we go on as before
            if (provCost <= currentCost && not_too_noisy(dataset, prov, eps_r, eps_c)) {
                core = prov;
                currentCost = provCost;
                currentPatterns = provPatterns;
            //otherwise we go back
            } else {
                provPatterns = currentPatterns;
            }
        }

        addedItem = false;
        provPatterns = currentPatterns;

        //until the extension list created in find_core is not empty
        while (!ext_list.empty()) {
            //we take out an item from the extension list
            int elem = ext_list.front();
            ext_list.pop();

            prov = core;

            //we add the extracted item to the candidate patttern
            prov.items.push_back(elem);
            //we update complexity by one because we added one item
            provPatterns.complexity++;

            //for each transactions in our candidate pattern
            for(auto &&tr : core.transactions) {
                //we check if the item is actually included in the transaction 
                bool included = (std::find(dataset[tr].items.begin(), dataset[tr].items.end(), elem) != dataset[tr].items.end());
                //we check if the item is already covered by another pattern 
                //(to do so we check if it is in the residuals if it is not, it is already covered)
                bool covered = (std::find(residuals[tr].items.begin(), residuals[tr].items.end(), elem) == residuals[tr].items.end());
                //if the item is not already covered by another pattern but it is included in the dataset
                //then we have one less false negative since thanks to the addition of the new item
                //that item-transaction pair is now covered
                if (!covered && included) {    
                    provPatterns.falseNegatives--;
                //otherwise if it is not included we increase by one false positives since what we are doing
                //is equivalent to cover an item-transaction pair that does not really exist.
                } else if (!included) {
                    provPatterns.falsePositives++;
                }
            }

            provCost = cost(provPatterns);

            //if the cost has improved and the new pattern is not too noisy
            //we keep the candidate and we go on as before
            if (provCost <= currentCost && not_too_noisy(dataset, prov, eps_r, eps_c)) {
                core = prov;
                currentCost = provCost;
                currentPatterns = provPatterns;
                addedItem = true;
                break;
            //otherwise we go back
            } else {
                provPatterns = currentPatterns;
            } 
        }
    }
    //std::cout << "not_too_noisy: " << noise1.count() + noise2.count() << std::endl << std::endl;
    
    //we update pattern quality
    possibleQuality = currentPatterns;
    return core;
}

pattern find_first(std::vector<int> sortedItems, datas residuals){
    pattern core;
    //we add to the core item set the most frequent item
    core.items.push_back(sortedItems[0]);

    //we add to the core transactions set all the transactions that contain the just added item
    for (auto &&r : residuals) {
        if (std::find(r.items.begin(), r.items.end(), sortedItems[0]) != r.items.end()) {
            core.transactions.push_back(r.index);
        }
    }
    return core;
}

pattern ext_first(pattern copy, datas residuals, int newItem, patterns_quality &provCopy){
    
    //previous number  of items, if we cut off a transaction
    //the items contained in it become uncovered again
    int itemsNum = copy.items.size(); 
    //we add the new candidate item
    copy.items.push_back(newItem);
    //here will be stored all the transactions that will not introduce
    //any false positive even if we added the new item
    std::vector<int> newTrans;
    
    //for every transactions stored up to now
    for (auto &&tr : copy.transactions) {
        //we check if there actually exist the pair new item-transaction in copy.transaction
        //if it does exist the transaction will be kept and so added to  newTrans. Moreover
        //we decrease by one false negatives because one item-transaction pair has just been
        //covered by a pattern
        if( std::find(residuals[tr].items.begin(), residuals[tr].items.end(), newItem) != residuals[tr].items.end()) {
            newTrans.push_back(tr); 
            provCopy.falseNegatives--;
        //otherwise we do not add the transaction to newTrans and we increment false negatives of
        //numItems i.e. of the number of items that were in the discarded transaction and that now
        //are no longer covered by the pattern.
        } else {
            provCopy.falseNegatives+=itemsNum;
        }        
    }
    //transactions of our dense core are given by vector newTrans
    copy.transactions = newTrans;
    //we update complexity
    provCopy.complexity=copy.transactions.size()+copy.items.size();
    return copy;
    
}


std::pair<pattern, std::queue<int>> find_core(datas residuals, patterns_quality &possibleQuality, datas dataset) {

    //std::chrono::duration<double> extFirstTime;

    //this will be the queue of all the items that will not be added in this procedure because they
    //would admit false positives (which we do not admit) but that we will use to try to extend our dense core
    std::queue<int> ext_list;
    //In this vector we will have all the items contained in the dataset ordered by frequency
    std::vector<int> sortedItems;
    pattern core, copy;
    patterns_quality provCore = possibleQuality, provCopy = possibleQuality;
    float coreCost, copyCost;

    //we sort the remaining items to cover by their frequency
    sortedItems = sorting(residuals); 

    //this function will add to the item set the most frequent item and
    //all the transactions it appears in
    core = find_first(sortedItems, residuals);
    //I can decrease falseNeg of core.transactions.size() because I am sure 
    //I have covered a single item in those transactions
    provCore.falseNegatives -= core.transactions.size();
    //new complexity is given by the complexity got up to this moment plus the size of
    //added transactions plus one that would be the only item inserted in the core item set
    provCore.complexity = core.transactions.size()+1+possibleQuality.complexity; //item is just one

    coreCost = cost(provCore);
    provCopy = provCore;

    //Now we begin: starting from the second most frequent item we try to add it
    //without creating any false positive and evaluating cost each time.
    for (int i=1; i<sortedItems.size(); i++) {

        int newItem = sortedItems[i];
        copy = core;
        provCopy = provCore; 

        //auto start = std::chrono::steady_clock::now();

        //with this call we extend the first found core with other items
        //being carefull not to add any false positive
        copy = ext_first(copy, residuals, newItem, provCopy);

        //auto end = std::chrono::steady_clock::now();
        //extFirstTime += std::chrono::duration_cast<std::chrono::duration<float>>(end-start);

        //we update complexity
        provCopy.complexity += possibleQuality.complexity; 
        //we compute the new cost
        copyCost = cost(provCopy);

        //if the cost is less than before we keep the candidate and we go on as before
        if (copyCost <= coreCost) {
            core = copy;
            coreCost = copyCost;
            provCore = provCopy;
        //otherwise we discard the candidate, put the candidate item in the
        //extension list and go on as before
        } else {
            ext_list.push(newItem);
        }
    }
    //we update complexity and false negative 
    //(no update of false positives because we do not accept any)
    possibleQuality.complexity = provCore.complexity;
    possibleQuality.falseNegatives = provCore.falseNegatives;

    //std::cout << "ext_first: " << extFirstTime.count() << std::endl << std::endl;

    return make_pair(core, ext_list);
}



pattern_set panda_plus(int k, datas dataset) {

    //std::chrono::duration<double> findCoreTime, extCoreTime, residualsTime;

    //this will be the final result, and where from time to time we add a pattern if it is worthy to add
    pattern_set patterns;

    //this will be the structures keeping track of the current quality of our set of patterns
    //and of the quality of the candidate set.
    patterns_quality currentQuality, possibleQuality;

    //here will be hold the result of function extend_core
    pattern ext_core;

    //these are the variables that will keep track of the cost of the current set of patterns
    //and of the candidate set of patterns cost
    float currentCost, possibleCost;

    //these are the maximum noise threshold, if they are equal to one they are actually disabled
    float eps_r=1, eps_c=1;

    //residuals at the beginning will be equal to the original dataset
    //since there is not portion of dataset covered by any pattern
    datas residuals = dataset;

    //at the beginning false negatives are all the element of the dataset since
    //no element of the dataset is contained in a pattern.
    possibleQuality.falseNegatives = get_size(dataset);
    currentCost = FLT_MAX;

    //we would like to find k patterns
    for (int i=0; i<k; i++) {
        

        //If residuals are empty it means that all data has been collected in the patterns 
        //consequently there is no more pattern to be found and we can return
        if (residuals_are_empty(residuals)) {
            return patterns;
        }

        //auto start = std::chrono::steady_clock::now();
        
        //first step is to extract a dense core
        auto [core, ext_list] = find_core(residuals, possibleQuality, dataset); 


        //auto end = std::chrono::steady_clock::now();
        //findCoreTime += std::chrono::duration_cast<std::chrono::duration<float>>(end-start);

        //start = std::chrono::steady_clock::now();

        //second step is to extend the dense core admitting false positives
        ext_core = extend_core(core, ext_list, possibleQuality, dataset, residuals, eps_r, eps_c);

        //end = std::chrono::steady_clock::now();
        //extCoreTime += std::chrono::duration_cast<std::chrono::duration<float>>(end-start);
       
        //we compute the cost of our patterns set as if we added to it the new candidate pattern
        possibleCost = cost(possibleQuality);

        //if my current cost is less than the cost of the new set I stop
        if (currentCost < possibleCost) {
            break;
        }
        

        //otherwise patterns includes the new pattern found, currentCost becomes possibleCost, 
        //currentQuality becomes the possibleQuality.
        patterns.p.push_back(ext_core);
        currentCost = possibleCost;
        currentQuality = possibleQuality;

        //start = std::chrono::steady_clock::now();

        //we compute the residuals, meaning the new dataset we will work on will be given by the previous dataset
        //except for all the pairs item-transaction covered by one of the patterns in patterns.
        get_residuals(patterns, residuals);
       

        //end = std::chrono::steady_clock::now();
        //residualsTime += std::chrono::duration_cast<std::chrono::duration<float>>(end-start);

        
    }
    
    //std::cout << "find_core: " << findCoreTime.count() << std::endl << std::endl;
    //std::cout << "ext_core: " << extCoreTime.count() << std::endl << std::endl;
    //std::cout << "get_residuals: " << residualsTime.count() << std::endl << std::endl;

    patterns.q = currentQuality;
    return patterns;
}
