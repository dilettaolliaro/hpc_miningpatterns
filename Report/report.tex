%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wenneker Assignment
% LaTeX Template
% Version 2.0 (12/1/2019)
%
% This template originates from:
% http://www.LaTeXTemplates.com
%
% Authors:
% Vel (vel@LaTeXTemplates.com)
% Frits Wenneker
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[11pt]{scrartcl} % Font size

\usepackage{hyperref}
\usepackage{graphicx}


\hypersetup{
    colorlinks=true,  
    urlcolor=blue,
    citecolor = black,
    linkcolor = black
}




\input{structure.tex} % Include the file specifying the document structure and custom commands

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\title{	
	\normalfont\normalsize
	\textsc{Ca' Foscari University of Venice}\\ % Your university, school and/or department name(s)
	\vspace{25pt} % Whitespace
	\rule{\linewidth}{0.5pt}\\ % Thin top horizontal rule
	\vspace{20pt} % Whitespace
	{\huge High Performance Computing\vspace{5mm}\\\textit{Project Report}}\\ % The assignment title
	\vspace{12pt} % Whitespace
	\rule{\linewidth}{2pt}\\ % Thick bottom horizontal rule
	\vspace{12pt} % Whitespace
}

\author{Diletta Olliaro\\ 855957}  % Your name



\date{\vspace{20pt}\today} % Today's date (\today) or a custom date

\begin{document}

\maketitle % Print the title



\section{Introduction}

In this report we are going to present our implementation of the pattern mining algorithm \texttt{\large{P}\small{A}\large{ND}\small{A}}$^{+}$. The main goal of this project is to try to optimize our code with different techniques and analyse where these modifications lead our program performances.\\
First, we will describe the general structure of this algorithm. Secondly, we will describe our first implementation and the obstacles we found which lead us to some interesting modification. Thirdly, we will explain the strategy used to parallelize this algorithm and the obtained results. Finally, we will discuss some more advanced optimization techniques that could be applied and present some ideas that could be interesting to explore in the future.

\section{General Structure}

Given a binary matrix describing some dataset the main objective of the  \texttt{\large{P}\small{A}\large{ND}\small{A}}$^{+}$ algorithm is finding a set of $k$ patterns that best describes the input data and at the same time minimize some cost function. A detailed description of the algorithm can be found in \cite{pandapaper}, here we will just generally present the algorithm structure.\\
An \textit{approximate pattern} is given by two sets of \textit{items} and \textit{transactions}, the final set  of found patterns will actually constitute a subset of the original matrix. Notice that these patterns may cover some item-transaction pairs that is not contained in the original dataset, these pairs will be denoted as \textit{false positives}, which we will accept up to a certain threshold.\\
The program is divided in two main functions \texttt{find\_core} and \texttt{extend\_core}.
\begin{itemize}
\item The first one extract from the dataset what we will call a \textit{dense core}, meaning a first attempt of pattern which will not admit false positives. In order to do this we first find the most frequent item and all the transactions it appears in, then we insert the item and its transactions in our provisional pattern. Afterwards, following a frequency order, we keep adding items and cutting off from the pattern those transactions that do not contain all the items inserted up to that moment. Every time we try to add an element, we check if the cost has increased or not; in the first case we keep the previous pattern and put the item we tried to insert in a separate list that we will use later on, otherwise we keep the current created pattern.
\item The second one iteratively tries to add transactions and items as long as the cost function is reduced. This time we bear some false positives up to a certain threshold. Meaning that differently from before, we do not cut off items-transactions pairs that do not actually appear in the original dataset but we will increase a counter for false positives that will contribute in the computation of the cost function. Every time we try to add an item or a transaction, we then check through functions \texttt{cost} and \texttt{not\_too\_noisy}, the cost has not increased (by comparison with the previous pattern cost) and that the new pattern does not exceed the noise threshold we are willing to accept.
\end{itemize} 
At each iteration the pattern that most minimizes the cost function is added to the solution and this proceedings is repeated until \textit{k} patterns are found or the cost function does not improve anymore. Finally notice that we work on the whole dataset only in the first iteration, after we work on a subset of it; the subset given by the elements that remain not covered from some found pattern.

\section{First Implementation}
For our first implementation we followed step by step the pseudo-code proposed in \cite{pandapaper}. To do so we created a \texttt{struct pattern} composed by two sets, one representing items and the other one for transactions. Since our understanding of the problem was still superficial we thought it was a viable path the one of computing the cost of a set of patterns every time from the beginning. As the computation of the cost requires every time the knowledge of false positives and false negatives we actually introduced an enormous overhead; which reflected in unbearable execution times.\\
At this point we had to change approach, in fact we realized false positives and negatives could be computed all along the process of creating patterns, to do so we created a new data structure called \texttt{pattern\_set} containing a vector of \texttt{pattern} and two integers corresponding to false positives and negatives.\\
Finished this first sequential implementation we had enormous execution times and this was provoked by the fact that every time we tried to add a new item or transaction we actually inserted them in a provisional copy of our current pattern set. Meaning every time we inserted them, computed the cost, if it improved the cost with respect to before then we copied the provisional pattern set in our current pattern set, otherwise we copied our current pattern set in the provisional one. Continuous insertions and copies were the main cause of our program overhead.
\section{First Optimizations}
From now on we will present some improvements performed on our code, we will also present some graphics to represent and confront execution times. Notice that execution times measurements are performed through the use of the C++ \texttt{chrono} library, collection of data (in order to have estimations of the average execution times) was performed using a python script which executed the program 20 times recording the time results. Moreover, the reader should know that our tests were performed on the test dataset \texttt{chess.dat} (source: \cite{datasource}).\\

Trying to improve the enormous execution times, we realized that it was not necessary to actually insert the item or the transaction in the pattern set data structure. It was enough to keep track of the cost modifications that the adding would cause, and to insert the new elements if and only if the cost was improved.
In order to do so we changed our data structures a little bit: instead of having the \texttt{struct pattern\_set} composed by a vector of patterns and two integers as before we now have two field; one unchanged , the vector of pattern and the other is a struct called \texttt{pattern\_quality}, the latter just contains three integers representing false positives, false negatives and complexity (in practice everything we need to compute the cost). This resulted in using this new data structure to perform all our evaluation and computation as a matter of fact we only modified the patterns quality and never the patterns them self unless we already decided to add an item or a transaction. In this way, it is surely cheaper to copy every time one or two integers and increment or decrement them than copy whole data structures and insert elements in a set.
This approach nearly halved our execution time that at this point was in average 60.997 seconds.\\
After this we considered changing from set to vector the data structure of the sets composing a pattern. This was done for two main reasons:
\begin{itemize}
\item Usually vectors result more efficient as they are allocated in contiguous memory locations.
\item Moreover, in a near future we will try to parallelize our code which can be implemented only with data structures that allow sequential access.
\end{itemize}
This led our execution time to an average of 53.790, meaning we actually obtain a second slight improvement. In fig. \ref{img1} we see a graph representing execution times of different pieces of code (the more important ones) in the sequential implementation using sets (yellow) confronted with the timings of the one using vectors (orange).  Notice that obviously some timings comprehend also the timing of another function, for example \texttt{panda} comprehends all the other, whereas \texttt{not\_too\_noisy} has to be considered  as part  of \texttt{ext\_core} and finally \texttt{ext\_first} is part of \texttt{find\_core}. We actually took the timings that had more influence on the overall total time.

\begin{figure}[H]
\centering
\includegraphics[width=12cm]{Images/img1.png}
\caption{Comparison of the executions times of various functions between two different implementations: one implementation presents sets (yellow) and the other one presents vectors (orange).}
\label{img1}
\end{figure}

\section{OpenMP Parallelization}
After these first modifications we wanted to try parallelize our code, to do so we used OpenMP\footnote{The OpenMP API supports multi-platform shared-memory parallel programming in C/C++ and Fortran.}. We encountered a main difficulty in this: the program presents a lot of data dependencies, so much that at first glance one could say it is not parallelizable at all. However even if we cannot parallelize externally, meaning the biggest tasks cannot be performed at the same time, we tried to parallelize something in the inner structure.\\ Knowing the two main tasks are \texttt{find\_core} and \texttt{extend\_core} we tried to discover which were the more expensive parts of these tasks, and they were \texttt{ext\_first} and \texttt{not\_too\_noisy}, respectively. Individuated these two critical parts we parallelized them obtaining nearly halved execution times as we can see in fig. \ref{img2}

\begin{figure}[H]
\centering
\includegraphics[width=12cm]{Images/img2.png}
\caption{Comparison of the executions times of various functions between two different implementations: one implementation is sequential (orange) and the other one is parallel (dark orange).}
\label{img2}
\end{figure}
Taking a closer look to timings we discovered that actually parallelizing also \texttt{ext\_first} led to a really slight decrease in the \texttt{find\_core} performances; probably because the creation/destruction of threads causes more overhead with respect to the benefits, in terms of time saving, of having work performed in parallel.\\
Finally notice that as we mentioned before we had to change a little bit our code, in particular the loop structure of the loops we wanted to parallelize. This because OpenMP parallelization requires subscript operator to sequentially access the data structure, and this functionality is not provided by the \texttt{for each} loops.\\
In conclusion the main problem was that our major tasks were not parallelizable (because we need residuals to extract a new dense core, but to get residuals we need to have found at least a pattern) because of dependencies; moreover we have a lot of nested loops that are knowingly more difficult to parallelize because it is only possible to use the \texttt{collapse} OpenMP directive if the loop body can consist only of the inner loop, meaning there can be no statements before or after the inner loop in the loop body of the outer loop and this was never our case. Consequently, what we need  to highlight is that only parallelizing \texttt{not\_too\_noisy} brought some speedup. 
\section{Optimization Obstacles}
At this point we wanted to see if following some of the "\textit{A guide to Vectorization with Intel C++ Compilers}" (\cite{autovec}) advices we could achieve even better results. To do so we tried to follow as much as we can the general guidelines, such as prefer countable single entry and single exit “for” loops, avoid switch and return statements, prefer array notation to the use of pointers, use the loop index directly in array subscripts where possible and so on. Unfortunately we could not satisfy all the guidelines because of how we constructed the code and also because of the algorithm structure, for example we could not avoid branches in some loops or the calling of some functions.\\
Once we modified our code, we tried to execute it with \texttt{-O3} flag, so that we can also see the loop vectorization report. The execution times were outstanding as a matter of fact thanks to the compilers optimizations we achieved a huge speed up that we show in fig. \ref{img3}.

\begin{figure}[H]
\centering
\includegraphics[width=12cm]{Images/img3.png}
\caption{Comparison of the executions times of various functions with different optimizations: one is only optimized through parallelization (dark orange) and the other also through compiler optimizations (red).}
\label{img3}
\end{figure}

Unfortunately we found out that this enormous speedup was not caused by our modifications to the code, as we analysed the loop vectorization report (obtained adding to the compiling line the following flags \texttt{-Rpass=loop-vectorize}) and discovered that actually only one loop was vectorized and all the others were not. To be more precise we also analysed the report about the inlining (flags \texttt{-Rpass=inline}), noticing that the compiler performed the inlining of a lot of library functions. Apart from the latter fact, the reasons this speedup should be looked for in some more complicated optimizations performed by the compiler, probably on the assembly.

\section{Conclusion and Future Works}

In conclusion we had some difficulties in actually optimizing our implementation of this algorithm; as far as parallelization is concerned this was due for sure to the huge task-dependencies present in this kind of algorithm as a matter of fact as we pointed out before no one of the algorithm subtasks can be performed until the previous one has given a result. Even so we were able to obtain halved execution times because of some changes in the used  data structures and mostly because of parallelization of a particularly time-taking piece of code.\\
As future works it should be interesting to perform some major change in the code in order to encounter more carefully the guidelines proposed by \cite{autovec}, for example we could try to change data structures preferring structure of arrays (SoA) over arrays of structure (AoS) as we actually did; furthermore we could also try to carry on a type analysis and try to use cheaper data types where it is possible. Finally it could be very interesting studying in detail all the increases or decreases in performances caused by these modifications.


\medskip

\begin{thebibliography}{9}
\bibitem{pandapaper} 
Claudio Lucchese, Salvatore Orlando, and Raffaele Perego. 
\textit{A Unifying Framework for Mining Approximate Top-k Binary Patterns}. 
IEEE Transactions on Knowledge and Data Engineering, Vol. 26, No. 12, December 2014.
\bibitem{datasource} \textit{Frequent Itemset Mining Dataset Repository}, \href{http://fimi.uantwerpen.be/data/}{http://fimi.uantwerpen.be/data/}
\bibitem{autovec} \textit{A Guide to Vectorization with Intel® C++ Compilers}


\end{thebibliography}

\end{document}
